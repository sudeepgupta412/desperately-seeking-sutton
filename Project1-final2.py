#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import random as rd
import matplotlib.pyplot as plt
#get_ipython().run_line_magic('pylab', 'inline')
#get_ipython().run_line_magic('matplotlib', 'inline')
#import seaborn as sns
import time


# In[2]:


num_states = 7
start_state = 3
num_sequence=10
num_training_set=100
sequence_limit = 26


# In[3]:


def initialize(num_states,num_sequence,num_training_set):
    ref_weight = np.ones((num_states-2,1))
    ref_weight[0][0] = np.float32(1*1/6)
    ref_weight[1][0] = np.float32(1*1/3)
    ref_weight[2][0] = np.float32(1*1/2)
    ref_weight[3][0] = np.float32(1*2/3)
    ref_weight[4][0] = np.float32(1*5/6)
    
    return ref_weight


# In[4]:


ref_weight = initialize(num_states,num_sequence,num_training_set)


# In[5]:


def generate_sequence(num_states, start_state,seed):
    step = 0
    X_train = np.zeros((num_states,1))
    #print(np.shape(X_train))
    current_state = start_state
    X_train[current_state]=1
    #print(X_train)
    
    
    #np.random.seed(seed)
    while (current_state > 0) and (current_state < num_states-1):
        
        step = step + 1
        x_train = np.zeros((num_states,1))
        prob_step = np.random.uniform()
        #print(prob_step)
        if prob_step <= 0.5:
            current_state = current_state -1
            #print(current_state)
        else:
            current_state = current_state + 1

        x_train[current_state] = 1
        #print(x_train)
        X_train = np.hstack([X_train,x_train])
        
    return (step, X_train)


# In[6]:


def training_set(num_states, start_state, num_sequence, num_training_set):
    trainX = [[None] * num_sequence] * num_training_set
    trainY = np.zeros((num_training_set,num_sequence))
    #print(np.shape(trainX))
    #print(np.shape(z))
    seedval = 30
    np.random.seed(seedval)
    seed = seedval
    #steps = sequence_limit + 1
    for i in range(num_training_set):
        for j in range(num_sequence):
            seedval = seedval + int(seedval**0.5)
            stop_condition = False
            steps = sequence_limit + 1
            while stop_condition == False:
                (steps, X_train) = generate_sequence(num_states, start_state,seed)
                #print(steps)
                if steps > sequence_limit:
                    stop_condition = False
                else:
                    stop_condition = True
            
            #print("i =" + str(i) + ", and j =" + str(j))
            trainX[i][j] = X_train
            trainY[i][j] = X_train[-1][-1]
    return (trainX, trainY) 


# In[7]:


(trainX, trainY) = training_set(num_states, start_state, num_sequence, num_training_set)
initial_w = np.ones((num_states-2,1))*0.5


# In[8]:


def calculate_et(t, lamda, weight, xs, num_states):
    #lamda_vector = np.zeros(len(sequence))
    et = np.zeros((num_states-2,1))
    t = t+1
    for k in range(1,t+1):
        i = k-1
#        et = et + lamda*et + xs[:,[i]]
        et = et + (lamda ** (t-k))*xs[:,[i]]
    return et
    


# In[9]:


def update_weight(sequence_x, zs, num_states, alpha, lamda, weight):
    #print("calculating weights ..")
    xs = sequence_x[1:-1,0:-1]
    mid_states = num_states -2
    steps = xs.shape[1]
    #zs = [0,1][int(sequence_x[-1, -1])]
    samples = sequence_x.shape[1]-1
    delta_p = 0
    delta_w = np.zeros((num_states-2,1))
    for t in range(samples):
        Pt = np.dot(weight.T,xs[:,[t]])
        Pt = Pt[0][0]
        if t == samples-1:
            Ptplus1 = zs
        else:
            Ptplus1 = np.dot(weight.T,xs[:,[t+1]])
            Ptplus1 = Ptplus1[0][0]
        
        delta_p = Ptplus1 - Pt
        et = calculate_et(t, lamda, weight, xs, num_states)
        #et = calculate_et(t, lamda, weight, sequence_x, num_states)
        delta_w += alpha * delta_p * et
    return delta_w
        


# In[10]:


def check_convergence(w_old, w_new, epsilon):
    value = np.sqrt(((w_old - w_new) ** 2).mean())
    
    if (value < epsilon):
        converged = True
    else:
        converged = False


# In[11]:


def experiment1(trainX, trainY, num_sequence, num_training_set, alpha, lamda, weight, num_states):
    error = 0
    epochs = 20
    dw = np.zeros((num_states-2,1))
    convergence_val = 0.01
    for i in range(num_training_set):
        
        convergence = False
        dw = np.zeros((num_states-2,1))
        while(convergence==False):
            old_w = weight.copy()
            for j in range(num_sequence):
                
                dw += update_weight(trainX[i][j], trainY[i][j], num_states, alpha, lamda, weight)
            weight += dw
            convergence = check_convergence(old_w,weight,convergence_val)
            
        error += np.sqrt(((np.round(weight, decimals=20) - ref_weight) ** 2).mean())
    error = error/num_training_set
    return error


# In[20]:

def run_exp1():
    start_time_exp1 = time.time()
    print("running exp 1 ..")
    error_vector = []
    lamdas = np.linspace(0,1,10)
    alpha = 0.01
    for i in range(len(lamdas)):
        start_time_lambda = time.time()
        weight = np.ones((num_states-2,1))*0.5
        lamda = lamdas[i]
        error = experiment1(trainX, trainY, num_sequence, num_training_set, alpha,lamda, weight, num_states) 
        #print(error)
        error_vector.append(error)
        print(str(i+1) + " of " + str(len(lamdas)) + " finished")
        #print("time taken to run for lambda " + str(lamda) + " = " + str(time.time()-start_time_lambda))  


    # In[24]:

    print("time taken to run exp 1 =" + str(time.time() - start_time_exp1))
    #print("average time taken to run exp 1 =" + str((time.time() - start_time_exp1)/(len(lamdas))))
    #print(error_vector[1])
    print("Plotting graphs for exp 1 ..")
    plt.figure(figsize=(10,8))
    plt.plot(lamdas,error_vector,'-o')
    plt.xlabel('λ',size=20)
    plt.ylabel('ERROR',size=20)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.title('Generated figure 3 of Sutton 1988',size=20)
    plt.annotate('Widrow-Hoff',(0.75, error_vector[-1]),fontsize=20)
    plt.show()


# In[25]:


def experiment2(trainX, trainY, num_sequence, num_training_set, alpha, lamda, weight, num_states):
    error = 0
    weight = np.ones((num_states-2,1))*0.5
    dw = np.zeros((num_states-2,1))
    
    for i in range(num_training_set):
        weight = np.ones((num_states-2,1))*0.5
        for j in range(num_sequence):
            
            zs = [0,1][int(trainX[i][j][-1, -1])]
            weight += update_weight(trainX[i][j], zs, num_states, alpha, lamda, weight)
        #weight += dw
            error += np.sqrt(((np.round(weight, decimals=20) - ref_weight) ** 2).mean())
    #error += (np.sum((ref_weight - weight)**2)/(num_states-2))**0.5
    #print("final weight = " + str(weight.T)) 
    error = error/(num_training_set*num_sequence)
    return error


# In[26]:

def run_exp2():
    start_time_exp2 = time.time()
    print("Running experiment 2 ..")
    lambda_vector = [1,0,0.8,0.3]
    alphas = np.linspace(0,0.7,20)
    error_vector = {1:[],0:[],0.8:[],0.3:[]}
    for lamda_val in lambda_vector:
        #start_time_lambda = time.time()
        print('Calculating for lambda = {}:'.format(lamda_val))
        for i in range(len(alphas)):
            alpha = alphas[i]
            weight = np.ones((num_states-2,1))*0.5
            lamda = lamda_val
            
            error_vector[lamda_val].append(experiment2(trainX, trainY, num_sequence, num_training_set, alpha,lamda, weight, num_states))
        #print("time taken to run for lambda " + str(lamda_val) + " = " + str(time.time()-start_time_lambda))  
    print("time taken to run exp 2 =" + str(time.time() - start_time_exp2))
    #print("average time taken to run exp 2 =" + str((time.time() - start_time_exp2)/(len(lambda_vector)*len(alphas))))
    print("Generating plots for exp2 ..")
    plt.figure(figsize=(8,8))

    for i in lambda_vector:
        plt.plot(alphas,np.asarray(error_vector[i]),'-o',label='λ = {}'.format(i))
        if i == 1:
            plt.annotate('λ = {} (Widrow-Hoff)'.format(i),(0.64,np.asarray(error_vector[i][-2])))
        else:    
            plt.annotate('λ = {}'.format(i),(0.72,error_vector[i][-1]))

    plt.title('Generated Figure 4 of Sutton 1988',size=15)
    plt.xlabel('α',size=20)
    plt.ylabel('ERROR',size=20)
    plt.ylim(top=0.6, bottom=0.0)
    plt.xlim([-0.05,0.8])
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xticks(np.linspace(0,0.7,8))
    plt.legend()
    plt.show()


# In[33]:

def run_exp3():
    start_time_exp3 = time.time()
    print("Running experiment 3 ..")
    alpha_b = 0.30 # based on Fig. 4
    lambda_vector = np.linspace(0,0.7,15)
    error_vector = []
    for i in range(len(lambda_vector)):
        lamda = lambda_vector[i]
        weight = np.ones((num_states-2,1))*0.5
        
        error_vector.append(experiment2(trainX, trainY, num_sequence, num_training_set, alpha_b,lamda, weight, num_states))
        


    # In[35]:
    print("time taken to run exp 3 =" + str(time.time() - start_time_exp3))
    #print("average time taken to run exp 3 =" + str((time.time() - start_time_exp3)/(len(lambda_vector))))
    print("Generating plots for exp 3 ..")
    plt.figure(figsize=(8,6))
    plt.title('Generated Figure 5 of Sutton 1988',size=15)
    plt.plot(lambda_vector,error_vector,'-o')
    plt.annotate('Widrow-Hoff',(0.50, (error_vector[-1])),fontsize=16)
    plt.xlabel('λ',size=20)
    plt.ylabel('ERROR USING BEST α',size=20)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.show()


# In[ ]:


def main():
    
    run_exp1()
    run_exp2()
    run_exp3()
    

if __name__ == '__main__':
    main()





