# Desperately Seeking Sutton

## Description
One aspect of research in reinforcement learning (or any scientific field) is the replication of previously published results. There are a few benefits you might reap from replicating papers. One benefit of replication is that it augments your understanding of the material. Another benefit is that it puts you in a good position both to extend existing literature and consider new contributions to your field. Replication is also often challenging. You may find that values of key parameters are missing, that described methods are ambiguous, or even that there are subtle errors. Sometimes obtaining the same pattern of results is not possible. For this project, you will read Richard Sutton’s 1988 paper Learning to Predict by the Methods of Temporal Differences. Then you will create an implementation and replication of the results found in figures 3, 4, and 5.


## How do I run the code?
	Go to main() function in Project1-final2.py
	uncomment the experiment to be run.
	the graphs will be generated for the corresponding runs.

	